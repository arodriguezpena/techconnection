//
//  AppDelegate.swift
//  TechConnection
//
//  Created by arodriguezp2003 on 08/18/2019.
//  Copyright (c) 2019 arodriguezp2003. All rights reserved.
//

import UIKit
import TechConnection
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        callme()
        return true
    }
    
    func callme() {
        let model = TCFactory().getModel()
        model.login(email: "eve.holt@reqres.in", password: "b") { (bs:IResponse) in
            if bs.success {
                if let dic = bs.data as? NSDictionary {
                    if let token = dic["token"] as? String {
                        print("Token: \(token)")
                    }
                }
                
            } else {
                print(bs.message!)
            }
            
        }
        model.getUsers(delay: 0) { (bs:IResponse) in
            if bs.success {
                if let array = bs.data as? NSArray {
                    print(array)
                }
                
            }
        }
//
        
    }

  

}

