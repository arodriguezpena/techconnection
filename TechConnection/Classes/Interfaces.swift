//
//  Interfaces.swift
//  Pods-TechConnection_Example
//
//  Created by Alejandro  Rodriguez on 8/18/19.
//

import Foundation

public enum CONFIG : String {
    case URL = "https://reqres.in"
    func toString() -> String {
        return self.rawValue
    }
}
@objc public class IResponse: NSObject {
    @objc public let success: Bool
    @objc public let message: String?
    @objc public let data: Any?
    
    @objc public init(success: Bool, message: String?, data:Any?) {
        self.success = success
        self.message = message
        self.data = data
    }
}

